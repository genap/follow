FROM node:8

# Binds to port 3001
EXPOSE 3001

# Working directory for application
RUN mkdir -p /usr/src/follow
WORKDIR /usr/src/follow

# Copy application data
COPY . .

# Expose files volume
RUN mkdir -p /usr/share/follow
VOLUME [ "/usr/share/follow" ]

# Expose config volume
RUN mkdir -p /usr/etc/follow
VOLUME [ "/usr/etc/follow" ]

#CMD ["./wait_for_it.sh", "postgres:5432", "--", "npm", "run", "start:dev"]
